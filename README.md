# README #

In this (private) repository we can share our email addresses, appointments and other things of interest 

## Next Appointment
* When? Sunday, 11th of December, 12-17pm.
* Where? Implexis Analytics, Lilienstraße 15, 22095 Hamburg (4th floor)
* Mobile Malte: 015784998406

## Competitions:
### Current
* outbrain: https://www.kaggle.com/c/outbrain-click-prediction
* two-sigma: https://www.kaggle.com/c/two-sigma-financial-modeling
* fish: https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring

### Previous
* Grupo Bimbo
* Santander
* ...

## Active Members:
* Birgit Johannes <Birgit.Johannes@gmx.net>
* Christian Mondorf <christian@mondorf.dk>
* Henning Bumann <henning.bumann@posteo.de>
* Levin Nickelsen <levinnickelsen@posteo.de>
* Malte Grosser <malte.grosser@googlemail.com>
* Pooja Saxena <nrjrasaxena@gmail.com> 
* Clara Marie Lüders <clara.marie.lueders@gmail.com>
* Pasqui Dente <pasqui.dente@gmail.com>
* Sam Chien <chiensam@gmail.com>
* Jurgen Riedel <jriedel@thescienceinstitute.com>

## Protocols

# 11.12.16
**Attendees**: Birgit, Pooja, Clara, Malte, Sam, Pasqui (via Skype)

**Decisions**: We don't focus on one language or on compedition. It is ok, if someone wants to code in R, Python, C++ or whatever. Also if someone doesn't like a specific challenge and is motivated to work on another one, this is totally ok. Of course we still try to work and organise together, but lets try to learn and be productive.

**Progress**: We were working on the fish challenge. It is about imageclassification of eight fishclasses. We loaded the data into R, made them greyscale and downscaled the resolution. We were also trying to get into the R package mxnet, which is a convolutional neural network, but we didn't have enough time..